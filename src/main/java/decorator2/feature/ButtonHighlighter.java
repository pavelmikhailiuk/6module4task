package decorator2.feature;

import java.awt.Button;

public interface ButtonHighlighter {
	void highlight(Button button);
}
