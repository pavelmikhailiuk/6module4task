package decorator2.feature.impl;

import java.awt.Button;
import java.awt.event.MouseListener;

import decorator2.feature.ButtonHighlighter;

public class ButtonHighlighterImpl implements ButtonHighlighter {

	private MouseListener mouseListener;

	public ButtonHighlighterImpl(MouseListener mouseListener) {
		this.mouseListener = mouseListener;
	}

	public void highlight(Button button) {
		button.addMouseListener(mouseListener);
	}
}
