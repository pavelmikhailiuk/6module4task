package decorator2.feature.impl;

import java.awt.Button;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ButtonHighlightingMouseListenerImpl implements MouseListener {

	public void mouseClicked(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		Button button = (Button) e.getSource();
		button.setBackground(Color.WHITE);
	}

	public void mouseExited(MouseEvent e) {
		Button button = (Button) e.getSource();
		button.setBackground(Color.GRAY);
	}

}
