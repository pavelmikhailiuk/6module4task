package decorator2.main;

import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import decorator2.feature.ButtonHighlighter;
import decorator2.feature.impl.ButtonHighlighterImpl;
import decorator2.feature.impl.ButtonHighlightingMouseListenerImpl;

public class Runner {
	public static void main(String[] args) {
		Frame frame = createFrame("Decorator pattern task");
		frame.add(createPanel());
		frame.setVisible(true);
	}

	private static Frame createFrame(String name) {
		Frame frame = new Frame();
		frame.setTitle(name);
		frame.setSize(400, 400);
		frame.setLayout(new FlowLayout());
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		return frame;
	}

	private static Panel createPanel() {
		Panel panel = new Panel();
		panel.setBackground(Color.GRAY);
		panel.add(createButton("Ok", true));
		panel.add(createButton("Cancel", false));
		return panel;
	}

	private static Button createButton(String name, boolean enchanced) {
		Button button = new Button(name);
		button.setBackground(Color.GRAY);
		if (enchanced) {
			ButtonHighlighter highlighter = new ButtonHighlighterImpl(new ButtonHighlightingMouseListenerImpl());
			highlighter.highlight(button);
		}
		return button;
	}
}
